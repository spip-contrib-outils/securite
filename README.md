# spip/security

Protection du site par blocage de certaines attaques.

## Compatibilité PHP

La version minimum requise est `PHP5.4`.

## Documentation

[spip.net - Écran de sécurité](https://www.spip.net/fr_article4200.html)
